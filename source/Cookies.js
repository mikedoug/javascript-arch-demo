"use strict";

export default class Cookies {
    set(name, value) {
        document.cookie = name + '=' + value;
    }
}