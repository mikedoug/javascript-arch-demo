"use strict";

import Cookies from './Cookies';

// Copy "ref" from query string into a cookie, removing it from the query string
export default function ReferralCookie() {
    const QueryString = require('query-string');
    const parsed = QueryString.parse(location.search);
    const cookies = new Cookies();

    if ("ref" in parsed) {
        cookies.set("ref", parsed["ref"] + "__" + Math.round((new Date()).getTime() / 1000));

        // Remove ref= from query parameter
        delete parsed['ref'];
        const query = QueryString.stringify(parsed);
        const url =
            location.origin + 
            location.pathname + 
            // Only add the ? if there are items in our parsed dictionary
            (query.length > 0 ? "?" + query : "") +
            location.hash;
        history.replaceState({}, "", url);
    }
}